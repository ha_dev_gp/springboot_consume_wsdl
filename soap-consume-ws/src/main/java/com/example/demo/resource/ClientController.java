package com.example.demo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.calculator.Add;
import com.example.demo.calculator.AddResponse;
import com.example.demo.calculator.Multiply;
import com.example.demo.calculator.MultiplyResponse;
import com.example.demo.client.SoapConnector;
import com.example.demo.constant.Constants;

@RestController
@RequestMapping(value = "/test")
public class ClientController {

	@Autowired
	private SoapConnector soapConnector;

	@GetMapping("/add")
	public String invokeSoapClientToGetAddResponse(
			@RequestParam(name = "a", required = false, defaultValue = "10") Integer intA,
			@RequestParam(name = "b", required = false, defaultValue = "10") Integer intB) {

		Add request = new Add();
		request.setIntA(intA);
		request.setIntB(intB);

		AddResponse response = (AddResponse) soapConnector.callWebService(request,
				Constants.CALCULATOR_SOAP_ACTION_ADD);
		String result = Integer.toString(response.getAddResult());

		return "Operation: ADD<br>Parm 1:" + intA + "<br> Parm 2:" + intB + "<br>Result:" + result;

	}

	@GetMapping("/multiply")
	public String invokeSoapClientToGetMultiplyResponse(
			@RequestParam(name = "a", required = false, defaultValue = "10") Integer intA,
			@RequestParam(name = "b", required = false, defaultValue = "10") Integer intB) {

		Multiply request = new Multiply();
		request.setIntA(intA);
		request.setIntB(intB);

		MultiplyResponse response = (MultiplyResponse) soapConnector.callWebService(request,
				Constants.CALCULATOR_SOAP_ACTION_MULTIPLY);
		String result = Integer.toString(response.getMultiplyResult());

		return "Operation: MULTIPLY<br>Parm 1:" + intA + "<br> Parm 2:" + intB + "<br>Result:" + result;

	}

}
