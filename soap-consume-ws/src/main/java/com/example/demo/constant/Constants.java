package com.example.demo.constant;

public class Constants {
	
	public static final String CALCULATOR_WSDL = "http://www.dneonline.com/calculator.asmx?WSDL";
	public static final String CALCULATOR_ENDPOINT = "http://www.dneonline.com/calculator.asmx";
	public static final String CALCULATOR_SOAP_ACTION_ADD = "http://tempuri.org/Add";
	public static final String CALCULATOR_SOAP_ACTION_MULTIPLY = "http://tempuri.org/Multiply";

}
