package com.example.demo;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*import com.example.demo.calculator.Add;
import com.example.demo.calculator.AddResponse;
import com.example.demo.client.SoapClient;
import com.example.demo.client.SoapConnector;
import com.example.demo.constant.Constants;*/
//import com.example.demo.country.GetCountries;
//import com.example.demo.country.GetCountriesResponse;


@SpringBootApplication
@RestController
public class SoapConsumeWsApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(SoapConsumeWsApplication.class, args);
	}

}
