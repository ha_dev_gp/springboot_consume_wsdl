package com.example.demo.client;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.example.demo.constant.Constants;

public class SoapConnector extends WebServiceGatewaySupport {
	 
	/*
    public Object callWebService(String url, Object request){
        return getWebServiceTemplate().marshalSendAndReceive(url, request, new SoapActionCallback(Constants.CALCULATOR_SOAP_ACTION_ADD));
    }
    */
    
    public Object callWebService(Object request, String soapAction) {
        return getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(soapAction));
    }
}

